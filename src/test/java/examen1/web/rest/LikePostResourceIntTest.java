package examen1.web.rest;

import examen1.V2App;

import examen1.domain.LikePost;
import examen1.repository.LikePostRepository;
import examen1.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static examen1.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LikePostResource REST controller.
 *
 * @see LikePostResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = V2App.class)
public class LikePostResourceIntTest {

    private static final Boolean DEFAULT_LIKE = false;
    private static final Boolean UPDATED_LIKE = true;

    @Autowired
    private LikePostRepository likePostRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restLikePostMockMvc;

    private LikePost likePost;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LikePostResource likePostResource = new LikePostResource(likePostRepository);
        this.restLikePostMockMvc = MockMvcBuilders.standaloneSetup(likePostResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LikePost createEntity(EntityManager em) {
        LikePost likePost = new LikePost()
            .like(DEFAULT_LIKE);
        return likePost;
    }

    @Before
    public void initTest() {
        likePost = createEntity(em);
    }

    @Test
    @Transactional
    public void createLikePost() throws Exception {
        int databaseSizeBeforeCreate = likePostRepository.findAll().size();

        // Create the LikePost
        restLikePostMockMvc.perform(post("/api/like-posts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(likePost)))
            .andExpect(status().isCreated());

        // Validate the LikePost in the database
        List<LikePost> likePostList = likePostRepository.findAll();
        assertThat(likePostList).hasSize(databaseSizeBeforeCreate + 1);
        LikePost testLikePost = likePostList.get(likePostList.size() - 1);
        assertThat(testLikePost.isLike()).isEqualTo(DEFAULT_LIKE);
    }

    @Test
    @Transactional
    public void createLikePostWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = likePostRepository.findAll().size();

        // Create the LikePost with an existing ID
        likePost.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLikePostMockMvc.perform(post("/api/like-posts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(likePost)))
            .andExpect(status().isBadRequest());

        // Validate the LikePost in the database
        List<LikePost> likePostList = likePostRepository.findAll();
        assertThat(likePostList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLikeIsRequired() throws Exception {
        int databaseSizeBeforeTest = likePostRepository.findAll().size();
        // set the field null
        likePost.setLike(null);

        // Create the LikePost, which fails.

        restLikePostMockMvc.perform(post("/api/like-posts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(likePost)))
            .andExpect(status().isBadRequest());

        List<LikePost> likePostList = likePostRepository.findAll();
        assertThat(likePostList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLikePosts() throws Exception {
        // Initialize the database
        likePostRepository.saveAndFlush(likePost);

        // Get all the likePostList
        restLikePostMockMvc.perform(get("/api/like-posts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(likePost.getId().intValue())))
            .andExpect(jsonPath("$.[*].like").value(hasItem(DEFAULT_LIKE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getLikePost() throws Exception {
        // Initialize the database
        likePostRepository.saveAndFlush(likePost);

        // Get the likePost
        restLikePostMockMvc.perform(get("/api/like-posts/{id}", likePost.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(likePost.getId().intValue()))
            .andExpect(jsonPath("$.like").value(DEFAULT_LIKE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLikePost() throws Exception {
        // Get the likePost
        restLikePostMockMvc.perform(get("/api/like-posts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLikePost() throws Exception {
        // Initialize the database
        likePostRepository.saveAndFlush(likePost);

        int databaseSizeBeforeUpdate = likePostRepository.findAll().size();

        // Update the likePost
        LikePost updatedLikePost = likePostRepository.findById(likePost.getId()).get();
        // Disconnect from session so that the updates on updatedLikePost are not directly saved in db
        em.detach(updatedLikePost);
        updatedLikePost
            .like(UPDATED_LIKE);

        restLikePostMockMvc.perform(put("/api/like-posts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLikePost)))
            .andExpect(status().isOk());

        // Validate the LikePost in the database
        List<LikePost> likePostList = likePostRepository.findAll();
        assertThat(likePostList).hasSize(databaseSizeBeforeUpdate);
        LikePost testLikePost = likePostList.get(likePostList.size() - 1);
        assertThat(testLikePost.isLike()).isEqualTo(UPDATED_LIKE);
    }

    @Test
    @Transactional
    public void updateNonExistingLikePost() throws Exception {
        int databaseSizeBeforeUpdate = likePostRepository.findAll().size();

        // Create the LikePost

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLikePostMockMvc.perform(put("/api/like-posts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(likePost)))
            .andExpect(status().isBadRequest());

        // Validate the LikePost in the database
        List<LikePost> likePostList = likePostRepository.findAll();
        assertThat(likePostList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLikePost() throws Exception {
        // Initialize the database
        likePostRepository.saveAndFlush(likePost);

        int databaseSizeBeforeDelete = likePostRepository.findAll().size();

        // Delete the likePost
        restLikePostMockMvc.perform(delete("/api/like-posts/{id}", likePost.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<LikePost> likePostList = likePostRepository.findAll();
        assertThat(likePostList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LikePost.class);
        LikePost likePost1 = new LikePost();
        likePost1.setId(1L);
        LikePost likePost2 = new LikePost();
        likePost2.setId(likePost1.getId());
        assertThat(likePost1).isEqualTo(likePost2);
        likePost2.setId(2L);
        assertThat(likePost1).isNotEqualTo(likePost2);
        likePost1.setId(null);
        assertThat(likePost1).isNotEqualTo(likePost2);
    }
}
