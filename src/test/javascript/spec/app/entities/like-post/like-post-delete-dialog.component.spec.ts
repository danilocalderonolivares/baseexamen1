/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { V2TestModule } from '../../../test.module';
import { LikePostDeleteDialogComponent } from 'app/entities/like-post/like-post-delete-dialog.component';
import { LikePostService } from 'app/entities/like-post/like-post.service';

describe('Component Tests', () => {
    describe('LikePost Management Delete Component', () => {
        let comp: LikePostDeleteDialogComponent;
        let fixture: ComponentFixture<LikePostDeleteDialogComponent>;
        let service: LikePostService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [V2TestModule],
                declarations: [LikePostDeleteDialogComponent]
            })
                .overrideTemplate(LikePostDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LikePostDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LikePostService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
