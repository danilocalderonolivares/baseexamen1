/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { V2TestModule } from '../../../test.module';
import { LikePostDetailComponent } from 'app/entities/like-post/like-post-detail.component';
import { LikePost } from 'app/shared/model/like-post.model';

describe('Component Tests', () => {
    describe('LikePost Management Detail Component', () => {
        let comp: LikePostDetailComponent;
        let fixture: ComponentFixture<LikePostDetailComponent>;
        const route = ({ data: of({ likePost: new LikePost(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [V2TestModule],
                declarations: [LikePostDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(LikePostDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(LikePostDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.likePost).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
