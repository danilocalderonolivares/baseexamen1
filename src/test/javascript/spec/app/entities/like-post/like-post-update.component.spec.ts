/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { V2TestModule } from '../../../test.module';
import { LikePostUpdateComponent } from 'app/entities/like-post/like-post-update.component';
import { LikePostService } from 'app/entities/like-post/like-post.service';
import { LikePost } from 'app/shared/model/like-post.model';

describe('Component Tests', () => {
    describe('LikePost Management Update Component', () => {
        let comp: LikePostUpdateComponent;
        let fixture: ComponentFixture<LikePostUpdateComponent>;
        let service: LikePostService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [V2TestModule],
                declarations: [LikePostUpdateComponent]
            })
                .overrideTemplate(LikePostUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LikePostUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LikePostService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new LikePost(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.likePost = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new LikePost();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.likePost = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
