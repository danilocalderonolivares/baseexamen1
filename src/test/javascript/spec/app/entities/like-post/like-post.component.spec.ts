/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { V2TestModule } from '../../../test.module';
import { LikePostComponent } from 'app/entities/like-post/like-post.component';
import { LikePostService } from 'app/entities/like-post/like-post.service';
import { LikePost } from 'app/shared/model/like-post.model';

describe('Component Tests', () => {
    describe('LikePost Management Component', () => {
        let comp: LikePostComponent;
        let fixture: ComponentFixture<LikePostComponent>;
        let service: LikePostService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [V2TestModule],
                declarations: [LikePostComponent],
                providers: []
            })
                .overrideTemplate(LikePostComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(LikePostComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LikePostService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new LikePost(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.likePosts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
