import { NgModule } from '@angular/core';

import { V2SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [V2SharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [V2SharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class V2SharedCommonModule {}
