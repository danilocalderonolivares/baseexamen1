import { IComment } from 'app/shared/model/comment.model';
import { IPost } from 'app/shared/model/post.model';
import { ILikePost } from 'app/shared/model/like-post.model';
import { ITag } from 'app/shared/model/tag.model';

export interface IUsuario {
    id?: number;
    nickname?: string;
    status?: boolean;
    borrado?: boolean;
    comments?: IComment[];
    posts?: IPost[];
    likePosts?: ILikePost[];
    tags?: ITag[];
}

export class Usuario implements IUsuario {
    constructor(
        public id?: number,
        public nickname?: string,
        public status?: boolean,
        public borrado?: boolean,
        public comments?: IComment[],
        public posts?: IPost[],
        public likePosts?: ILikePost[],
        public tags?: ITag[]
    ) {
        this.status = this.status || false;
        this.borrado = this.borrado || false;
    }
}
