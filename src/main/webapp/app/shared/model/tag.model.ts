import { IPost } from 'app/shared/model/post.model';
import { IUsuario } from 'app/shared/model/usuario.model';

export interface ITag {
    id?: number;
    name?: string;
    status?: boolean;
    posts?: IPost[];
    usuarios?: IUsuario[];
}

export class Tag implements ITag {
    constructor(public id?: number, public name?: string, public status?: boolean, public posts?: IPost[], public usuarios?: IUsuario[]) {
        this.status = this.status || false;
    }
}
