import { Moment } from 'moment';
import { IUsuario } from 'app/shared/model/usuario.model';
import { IPost } from 'app/shared/model/post.model';

export interface IComment {
    id?: number;
    comment?: string;
    timestamp?: Moment;
    status?: boolean;
    usuario?: IUsuario;
    post?: IPost;
}

export class Comment implements IComment {
    constructor(
        public id?: number,
        public comment?: string,
        public timestamp?: Moment,
        public status?: boolean,
        public usuario?: IUsuario,
        public post?: IPost
    ) {
        this.status = this.status || false;
    }
}
