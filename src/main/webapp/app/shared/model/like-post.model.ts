import { IUsuario } from 'app/shared/model/usuario.model';
import { IPost } from 'app/shared/model/post.model';

export interface ILikePost {
    id?: number;
    like?: boolean;
    usuario?: IUsuario;
    post?: IPost;
}

export class LikePost implements ILikePost {
    constructor(public id?: number, public like?: boolean, public usuario?: IUsuario, public post?: IPost) {
        this.like = this.like || false;
    }
}
