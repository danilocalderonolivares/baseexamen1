import { Moment } from 'moment';
import { IComment } from 'app/shared/model/comment.model';
import { ILikePost } from 'app/shared/model/like-post.model';
import { ITag } from 'app/shared/model/tag.model';
import { IUsuario } from 'app/shared/model/usuario.model';

export interface IPost {
    id?: number;
    title?: string;
    text?: string;
    status?: boolean;
    imagen?: string;
    timestamp?: Moment;
    comments?: IComment[];
    likePosts?: ILikePost[];
    tag?: ITag;
    usuario?: IUsuario;
}

export class Post implements IPost {
    constructor(
        public id?: number,
        public title?: string,
        public text?: string,
        public status?: boolean,
        public imagen?: string,
        public timestamp?: Moment,
        public comments?: IComment[],
        public likePosts?: ILikePost[],
        public tag?: ITag,
        public usuario?: IUsuario
    ) {
        this.status = this.status || false;
    }
}
