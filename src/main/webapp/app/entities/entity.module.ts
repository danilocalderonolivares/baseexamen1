import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'usuario',
                loadChildren: './usuario/usuario.module#V2UsuarioModule'
            },
            {
                path: 'post',
                loadChildren: './post/post.module#V2PostModule'
            },
            {
                path: 'comment',
                loadChildren: './comment/comment.module#V2CommentModule'
            },
            {
                path: 'tag',
                loadChildren: './tag/tag.module#V2TagModule'
            },
            {
                path: 'like-post',
                loadChildren: './like-post/like-post.module#V2LikePostModule'
            },
            {
                path: 'usuario',
                loadChildren: './usuario/usuario.module#V2UsuarioModule'
            },
            {
                path: 'post',
                loadChildren: './post/post.module#V2PostModule'
            },
            {
                path: 'comment',
                loadChildren: './comment/comment.module#V2CommentModule'
            },
            {
                path: 'tag',
                loadChildren: './tag/tag.module#V2TagModule'
            },
            {
                path: 'like-post',
                loadChildren: './like-post/like-post.module#V2LikePostModule'
            },
            {
                path: 'usuario',
                loadChildren: './usuario/usuario.module#V2UsuarioModule'
            },
            {
                path: 'post',
                loadChildren: './post/post.module#V2PostModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class V2EntityModule {}
