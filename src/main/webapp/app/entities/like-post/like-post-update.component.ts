import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ILikePost } from 'app/shared/model/like-post.model';
import { LikePostService } from './like-post.service';
import { IUsuario } from 'app/shared/model/usuario.model';
import { UsuarioService } from 'app/entities/usuario';
import { IPost } from 'app/shared/model/post.model';
import { PostService } from 'app/entities/post';

@Component({
    selector: 'jhi-like-post-update',
    templateUrl: './like-post-update.component.html'
})
export class LikePostUpdateComponent implements OnInit {
    likePost: ILikePost;
    isSaving: boolean;

    usuarios: IUsuario[];

    posts: IPost[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected likePostService: LikePostService,
        protected usuarioService: UsuarioService,
        protected postService: PostService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ likePost }) => {
            this.likePost = likePost;
        });
        this.usuarioService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUsuario[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUsuario[]>) => response.body)
            )
            .subscribe((res: IUsuario[]) => (this.usuarios = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.postService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IPost[]>) => mayBeOk.ok),
                map((response: HttpResponse<IPost[]>) => response.body)
            )
            .subscribe((res: IPost[]) => (this.posts = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.likePost.id !== undefined) {
            this.subscribeToSaveResponse(this.likePostService.update(this.likePost));
        } else {
            this.subscribeToSaveResponse(this.likePostService.create(this.likePost));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ILikePost>>) {
        result.subscribe((res: HttpResponse<ILikePost>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUsuarioById(index: number, item: IUsuario) {
        return item.id;
    }

    trackPostById(index: number, item: IPost) {
        return item.id;
    }
}
