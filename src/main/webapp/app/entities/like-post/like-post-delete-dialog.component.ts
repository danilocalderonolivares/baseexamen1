import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ILikePost } from 'app/shared/model/like-post.model';
import { LikePostService } from './like-post.service';

@Component({
    selector: 'jhi-like-post-delete-dialog',
    templateUrl: './like-post-delete-dialog.component.html'
})
export class LikePostDeleteDialogComponent {
    likePost: ILikePost;

    constructor(protected likePostService: LikePostService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.likePostService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'likePostListModification',
                content: 'Deleted an likePost'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-like-post-delete-popup',
    template: ''
})
export class LikePostDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ likePost }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(LikePostDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.likePost = likePost;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/like-post', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/like-post', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
