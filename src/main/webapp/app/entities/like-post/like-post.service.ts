import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ILikePost } from 'app/shared/model/like-post.model';

type EntityResponseType = HttpResponse<ILikePost>;
type EntityArrayResponseType = HttpResponse<ILikePost[]>;

@Injectable({ providedIn: 'root' })
export class LikePostService {
    public resourceUrl = SERVER_API_URL + 'api/like-posts';

    constructor(protected http: HttpClient) {}

    create(likePost: ILikePost): Observable<EntityResponseType> {
        return this.http.post<ILikePost>(this.resourceUrl, likePost, { observe: 'response' });
    }

    update(likePost: ILikePost): Observable<EntityResponseType> {
        return this.http.put<ILikePost>(this.resourceUrl, likePost, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ILikePost>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ILikePost[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
