import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ILikePost } from 'app/shared/model/like-post.model';

@Component({
    selector: 'jhi-like-post-detail',
    templateUrl: './like-post-detail.component.html'
})
export class LikePostDetailComponent implements OnInit {
    likePost: ILikePost;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ likePost }) => {
            this.likePost = likePost;
        });
    }

    previousState() {
        window.history.back();
    }
}
