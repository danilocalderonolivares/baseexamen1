import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { V2SharedModule } from 'app/shared';
import {
    LikePostComponent,
    LikePostDetailComponent,
    LikePostUpdateComponent,
    LikePostDeletePopupComponent,
    LikePostDeleteDialogComponent,
    likePostRoute,
    likePostPopupRoute
} from './';

const ENTITY_STATES = [...likePostRoute, ...likePostPopupRoute];

@NgModule({
    imports: [V2SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        LikePostComponent,
        LikePostDetailComponent,
        LikePostUpdateComponent,
        LikePostDeleteDialogComponent,
        LikePostDeletePopupComponent
    ],
    entryComponents: [LikePostComponent, LikePostUpdateComponent, LikePostDeleteDialogComponent, LikePostDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class V2LikePostModule {}
