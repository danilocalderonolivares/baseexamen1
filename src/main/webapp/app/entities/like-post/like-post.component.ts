import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ILikePost } from 'app/shared/model/like-post.model';
import { AccountService } from 'app/core';
import { LikePostService } from './like-post.service';

@Component({
    selector: 'jhi-like-post',
    templateUrl: './like-post.component.html'
})
export class LikePostComponent implements OnInit, OnDestroy {
    likePosts: ILikePost[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected likePostService: LikePostService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.likePostService
            .query()
            .pipe(
                filter((res: HttpResponse<ILikePost[]>) => res.ok),
                map((res: HttpResponse<ILikePost[]>) => res.body)
            )
            .subscribe(
                (res: ILikePost[]) => {
                    this.likePosts = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInLikePosts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ILikePost) {
        return item.id;
    }

    registerChangeInLikePosts() {
        this.eventSubscriber = this.eventManager.subscribe('likePostListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
