import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { LikePost } from 'app/shared/model/like-post.model';
import { LikePostService } from './like-post.service';
import { LikePostComponent } from './like-post.component';
import { LikePostDetailComponent } from './like-post-detail.component';
import { LikePostUpdateComponent } from './like-post-update.component';
import { LikePostDeletePopupComponent } from './like-post-delete-dialog.component';
import { ILikePost } from 'app/shared/model/like-post.model';

@Injectable({ providedIn: 'root' })
export class LikePostResolve implements Resolve<ILikePost> {
    constructor(private service: LikePostService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILikePost> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<LikePost>) => response.ok),
                map((likePost: HttpResponse<LikePost>) => likePost.body)
            );
        }
        return of(new LikePost());
    }
}

export const likePostRoute: Routes = [
    {
        path: '',
        component: LikePostComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LikePosts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: LikePostDetailComponent,
        resolve: {
            likePost: LikePostResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LikePosts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: LikePostUpdateComponent,
        resolve: {
            likePost: LikePostResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LikePosts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: LikePostUpdateComponent,
        resolve: {
            likePost: LikePostResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LikePosts'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const likePostPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: LikePostDeletePopupComponent,
        resolve: {
            likePost: LikePostResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'LikePosts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
