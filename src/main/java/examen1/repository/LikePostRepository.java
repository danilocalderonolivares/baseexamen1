package examen1.repository;

import examen1.domain.LikePost;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the LikePost entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LikePostRepository extends JpaRepository<LikePost, Long> {

}
