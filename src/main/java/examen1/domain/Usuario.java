package examen1.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Usuario.
 */
@Entity
@Table(name = "usuario")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nickname", nullable = false)
    private String nickname;

    @NotNull
    @Column(name = "status", nullable = false)
    private Boolean status;

    @NotNull
    @Column(name = "borrado", nullable = false)
    private Boolean borrado;

    @OneToMany(mappedBy = "usuario")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comment> comments = new HashSet<>();
    @OneToMany(mappedBy = "usuario")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Post> posts = new HashSet<>();
    @OneToMany(mappedBy = "usuario")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<LikePost> likePosts = new HashSet<>();
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "usuario_tag",
               joinColumns = @JoinColumn(name = "usuario_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private Set<Tag> tags = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public Usuario nickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Boolean isStatus() {
        return status;
    }

    public Usuario status(Boolean status) {
        this.status = status;
        return this;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean isBorrado() {
        return borrado;
    }

    public Usuario borrado(Boolean borrado) {
        this.borrado = borrado;
        return this;
    }

    public void setBorrado(Boolean borrado) {
        this.borrado = borrado;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Usuario comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public Usuario addComment(Comment comment) {
        this.comments.add(comment);
        comment.setUsuario(this);
        return this;
    }

    public Usuario removeComment(Comment comment) {
        this.comments.remove(comment);
        comment.setUsuario(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public Usuario posts(Set<Post> posts) {
        this.posts = posts;
        return this;
    }

    public Usuario addPost(Post post) {
        this.posts.add(post);
        post.setUsuario(this);
        return this;
    }

    public Usuario removePost(Post post) {
        this.posts.remove(post);
        post.setUsuario(null);
        return this;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    public Set<LikePost> getLikePosts() {
        return likePosts;
    }

    public Usuario likePosts(Set<LikePost> likePosts) {
        this.likePosts = likePosts;
        return this;
    }

    public Usuario addLikePost(LikePost likePost) {
        this.likePosts.add(likePost);
        likePost.setUsuario(this);
        return this;
    }

    public Usuario removeLikePost(LikePost likePost) {
        this.likePosts.remove(likePost);
        likePost.setUsuario(null);
        return this;
    }

    public void setLikePosts(Set<LikePost> likePosts) {
        this.likePosts = likePosts;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public Usuario tags(Set<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public Usuario addTag(Tag tag) {
        this.tags.add(tag);
        tag.getUsuarios().add(this);
        return this;
    }

    public Usuario removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getUsuarios().remove(this);
        return this;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Usuario usuario = (Usuario) o;
        if (usuario.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), usuario.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Usuario{" +
            "id=" + getId() +
            ", nickname='" + getNickname() + "'" +
            ", status='" + isStatus() + "'" +
            ", borrado='" + isBorrado() + "'" +
            "}";
    }
}
