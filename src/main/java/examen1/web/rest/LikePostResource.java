package examen1.web.rest;
import examen1.domain.LikePost;
import examen1.repository.LikePostRepository;
import examen1.web.rest.errors.BadRequestAlertException;
import examen1.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing LikePost.
 */
@RestController
@RequestMapping("/api")
public class  LikePostResource {

    private final Logger log = LoggerFactory.getLogger(LikePostResource.class);

    private static final String ENTITY_NAME = "likePost";

    private final LikePostRepository likePostRepository;

    public LikePostResource(LikePostRepository likePostRepository) {
        this.likePostRepository = likePostRepository;
    }

    /**
     * POST  /like-posts : Create a new likePost.
     *
     * @param likePost the likePost to create
     * @return the ResponseEntity with status 201 (Created) and with body the new likePost, or with status 400 (Bad Request) if the likePost has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/like-posts")
    public ResponseEntity<LikePost> createLikePost(@Valid @RequestBody LikePost likePost) throws URISyntaxException {
        log.debug("REST request to save LikePost : {}", likePost);
        if (likePost.getId() != null) {
            throw new BadRequestAlertException("A new likePost cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LikePost result = likePostRepository.save(likePost);
        return ResponseEntity.created(new URI("/api/like-posts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /like-posts : Updates an existing likePost.
     *
     * @param likePost the likePost to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated likePost,
     * or with status 400 (Bad Request) if the likePost is not valid,
     * or with status 500 (Internal Server Error) if the likePost couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/like-posts")
    public ResponseEntity<LikePost> updateLikePost(@Valid @RequestBody LikePost likePost) throws URISyntaxException {
        log.debug("REST request to update LikePost : {}", likePost);
        if (likePost.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LikePost result = likePostRepository.save(likePost);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, likePost.getId().toString()))
            .body(result);
    }

    /**
     * GET  /like-posts : get all the likePosts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of likePosts in body
     */
    @GetMapping("/like-posts")
    public List<LikePost> getAllLikePosts() {
        log.debug("REST request to get all LikePosts");
        return likePostRepository.findAll();
    }

    /**
     * GET  /like-posts/:id : get the "id" likePost.
     *
     * @param id the id of the likePost to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the likePost, or with status 404 (Not Found)
     */
    @GetMapping("/like-posts/{id}")
    public ResponseEntity<LikePost> getLikePost(@PathVariable Long id) {
        log.debug("REST request to get LikePost : {}", id);
        Optional<LikePost> likePost = likePostRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(likePost);
    }

    /**
     * DELETE  /like-posts/:id : delete the "id" likePost.
     *
     * @param id the id of the likePost to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/like-posts/{id}")
    public ResponseEntity<Void> deleteLikePost(@PathVariable Long id) {
        log.debug("REST request to delete LikePost : {}", id);
        likePostRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
